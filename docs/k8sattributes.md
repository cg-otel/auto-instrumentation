The currently supported metadata keys are:

- k8s.pod.name
- k8s.pod.uid
- k8s.deployment.name
- k8s.node.name
- k8s.namespace.name
- k8s.pod.start_time
- k8s.replicaset.name
- k8s.replicaset.uid
- k8s.daemonset.name
- k8s.daemonset.uid
- k8s.job.name
- k8s.job.uid
- k8s.cronjob.name
- k8s.statefulset.name
- k8s.statefulset.uid
- k8s.container.name
- container.image.name
- container.image.tag
- container.id

By default, if metadata is not specified, the following fields are extracted and added to spans, metrics, and logs as resource attributes:

- k8s.pod.name
- k8s.pod.uid
- k8s.pod.start_time
- k8s.namespace.name
- k8s.node.name
- k8s.deployment.name (if the pod is controlled by a deployment)
- k8s.container.name (requires an additional attribute to be set: - container.id)
- container.image.name (requires one of the following additional attributes to be set: container.id or - k8s.container.name)
- container.image.tag (requires one of the following additional attributes to be set: container.id or - k8s.container.name)