## Python Runtime Metrics

The following metrics are collected by default after enabling - runtime metrics:

- runtime.python.cpu.time.sys
<br>Number of seconds executing in the kernel

- runtime.python.cpu.time.user
<br/>Number of seconds executing outside the kernel

- runtime.python.cpu.percent
<br/>CPU utilization percentage

- runtime.python.cpu.ctx_switch.voluntary
<br/>Number of voluntary context switches

- runtime.python.cpu.ctx_switch.involuntary
<br/>Number of involuntary context switches

- runtime.python.gc.count.gen0
<br/>Number of generation 0 objects

- runtime.python.gc.count.gen1
<br/>Number of generation 1 objects

- runtime.python.gc.count.gen2
<br/>Number of generation 2 objects

- runtime.python.mem.rss
<br/>Resident set memory

- runtime.python.thread_count
<br/>Number of threads
