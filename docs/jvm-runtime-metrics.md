## - jvm Runtime Metrics

The following metrics are collected by default after enabling runtime metrics:

- jvm.heap_memory
<br/>The total Java heap memory used.

- jvm.heap_memory_committed
<br/>The total Java heap memory committed to be used.

- jvm.heap_memory_init
<br/>The initial Java heap memory allocated.

- jvm.heap_memory_max
<br/>The maximum Java heap memory available.

- jvm.non_heap_memory
<br/>The total Java non-heap memory used. Non-heap memory is calculated as follows: Metaspace + CompressedClassSpace + CodeCache

- jvm.non_heap_memory_committed
<br/>The total Java non-heap memory committed to be used.

- jvm.non_heap_memory_init
<br/>The initial Java non-heap memory allocated.

- jvm.non_heap_memory_max
<br/>The maximum Java non-heap memory available.

- jvm.thread_count
<br/>The number of live threads.

- jvm.gc.cms.count
<br/>The total number of garbage collections that have occurred.

- jvm.gc.major_collection_count
<br/>The rate of major garbage collections. Set new_gc_metrics: true to receive this metric.

- jvm.gc.minor_collection_count
<br/>The rate of minor garbage collections. Set new_gc_metrics: true to receive this metric.

- jvm.gc.parnew.time
<br/>The approximate accumulated garbage collection time elapsed.
Shown as millisecond

- jvm.gc.major_collection_time
<br/>The fraction of time spent in major garbage collection. Set new_gc_metrics: true to receive this metric.
Shown as permille

- jvm.gc.minor_collection_time
<br/>The fraction of time spent in minor garbage collection. Set new_gc_metrics: true to receive this metric.
Shown as permille
