### Helm install from script
```sh
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3

chmod 700 get_helm.sh

./get_helm.sh
```
### Helm version
```sh
helm version
```
### Get nodes
```sh
kubectl get node -o wide
```
### OpenTelemetry Operator Helm Chart for cluster level metrics
```sh
git clone https://gitlab.com/cg-otel/auto-instrumentation.git

cd auto-instrumentation/opentelemetry-helm-charts-main/hostmetrics-helm-chart/

# Operator
helm install opentelemetry-operator opentelemetry-operator/

# Collector
helm install opentelemetry-collector opentelemetry-collector/
```
### Terraform using Helm provider for namespace instrumentation
```sh
#From root folder of git repository
cd opentelemetry-helm-charts-main/ns-instrumentation-helm-chart/opentelemetry-collector

terraform init

# Review variables.tf before apply
terraform apply
```

***Alternative method using Helm without Terraform***

### OpenTelemetry Operator Helm Chart for opentelemetry auto instrumentation
Note: Namespace should be available before installing the Helm chart.
```sh
#From root folder of git repository
cd opentelemetry-helm-charts-main/ns-instrumentation-helm-chart/opentelemetry-collector

# Review values.yaml before helm install
helm install opentelemetry-collector opentelemetry-collector/ -n examplenamespace-1

# Review values.yaml before helm install
helm install opentelemetry-collector opentelemetry-collector/ -n examplenamespace-2
```
<br/>

---
***References:***
### OpenTelemetry Collector [APM Only]
```sh
# opentelemetry-apm-collector.yml
apiVersion: opentelemetry.io/v1alpha1
kind: OpenTelemetryCollector
metadata:
  name: apm
spec:
  mode: daemonset
  hostNetwork: true
 
  config: |
    receivers:
      otlp:
        protocols:
          http:

    processors:
      batch:
        send_batch_max_size: 100
        send_batch_size: 10
        timeout: 10s
      memory_limiter: null

    exporters:
      datadog/1:
        api:
          site: datadoghq.com	
          key: "e9f690f62ffbf0c41c9354649ff0f005"

      datadog/2:
        api:
          site: datadoghq.com	
          key: "25e72d23f81dafb038fdedbb4ac62847"

    service:
      pipelines:
        traces:
          receivers: [ otlp ]
          processors: [ batch ]
          exporters: [ datadog/1, datadog/2 ]
        logs:
          receivers: [ otlp ]
          processors: [ batch ]
          exporters: [ datadog/1, datadog/2 ]
        metrics:
          receivers: [ otlp ]
          processors: [ batch ]
          exporters: [ datadog/1, datadog/2 ]
```
### OpenTelemetry Collector [Hostmetrics Only]
```sh
# opentelemetry-hostmetrics-collector.yml
apiVersion: opentelemetry.io/v1alpha1
kind: OpenTelemetryCollector
metadata:
  name: hostmetrics
spec:
  mode: daemonset
  hostNetwork: false
 
  config: |
    receivers:
      # The hostmetrics receiver is required to get correct infrastructure metrics in Datadog.
      hostmetrics:
        collection_interval: 10s
        scrapers:
          paging:
            metrics:
              system.paging.utilization:
                enabled: true
          cpu:
            metrics:
              system.cpu.utilization:
                enabled: true
          disk:
          filesystem:
            metrics:
              system.filesystem.utilization:
                enabled: true
          load:
          memory:
          network:
          processes:
        kubeletMetrics:
          enabled: true
        clusterMetrics:
          enabled: true
        kubernetesEvents:
          enabled: true
        kubernetesAttributes:
          enabled: true
        networkMonitoring:
          enabled: true
        logCollection:
          enabled: true
          containerCollectAll: true

    processors:
      batch:
        send_batch_max_size: 100
        send_batch_size: 10
        timeout: 10s
      memory_limiter: null

    exporters:
      datadog/1:
        api:
          site: datadoghq.com	
          key: "e9f690f62ffbf0c41c9354649ff0f005"

      datadog/2:
        api:
          site: datadoghq.com	
          key: "25e72d23f81dafb038fdedbb4ac62847"

    service:
      pipelines:
        metrics:
          receivers: [ hostmetrics ]
          processors: [ batch ]
          exporters: [ datadog/1, datadog/2 ]
```
### OpenTelemery Collector [aio]
aio stands for All in One
- Metrics, logs/entity_events, logs and traces are exported to DCCPL organisation in Datadog
- Metrics only is exported to DCCPL-1 organisation in Datadog 
```sh
# otel-collector.yml
---
#Service Account
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    app: otel
  name: otel

---
# RBAC
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  labels:
    app: otel
  name: otel
  namespace: default
rules:
- apiGroups:
  - ""
  resources:
  - events
  - namespaces
  - namespaces/status
  - nodes
  - nodes/spec
  - pods
  - pods/status
  - replicationcontrollers
  - replicationcontrollers/status
  - resourcequotas
  - services
  - clusterresourcequotas
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - apps
  resources:
  - daemonsets
  - deployments
  - replicasets
  - statefulsets
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - extensions
  resources:
  - daemonsets
  - deployments
  - replicasets
  verbs:
  - get
  - list
  - watch
- apiGroups:
  - batch
  resources:
  - jobs
  - cronjobs
  verbs:
  - get
  - list
  - watch
- apiGroups:
    - autoscaling
  resources:
    - horizontalpodautoscalers
  verbs:
    - get
    - list
    - watch
- apiGroups: 
  - "events.k8s.io"
  resources:
  - events
  verbs:
  - watch
  - list

---
# RBAC
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  labels:
    app: otel
  name: otel-collector
  namespace: default
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: otel-collector
  namespace: default

---
apiVersion: opentelemetry.io/v1alpha1
kind: OpenTelemetryCollector
metadata:
  name: otel
  labels:
    app: otel
  namespace: default
spec:
  mode: daemonset
  hostNetwork: false

  config: |
    processors:
      resourcedetection:
        detectors: [env, system]
      cumulativetodelta:
      batch:
        send_batch_max_size: 100
        send_batch_size: 10
        timeout: 10s
      memory_limiter: null
      
    receivers:
      otlp:
        protocols:
          http:

      # The hostmetrics receiver is required to get correct infrastructure metrics in Datadog.
      hostmetrics:
        collection_interval: 10s
        scrapers:
          paging:
            metrics:
              system.paging.utilization:
                enabled: true
          cpu:
            metrics:
              system.cpu.utilization:
                enabled: true
          disk:
          filesystem:
            metrics:
              system.filesystem.utilization:
                enabled: true
          load:
          memory:
          network:
          processes:

      prometheus:
        config:
          scrape_configs:
          - job_name: 'otel'
            scrape_interval: 10s
            static_configs:
            - targets: ['0.0.0.0:8888']

      k8s_events:
        auth_type: serviceAccount

      k8sobjects:
        auth_type: serviceAccount

      k8s_cluster:
        auth_type: serviceAccount
        node_conditions_to_report:
          - Ready
          - MemoryPressure
        allocatable_types_to_report:
          - cpu
          - memory
        metrics:
          k8s.container.cpu_limit:
            enabled: false
        resource_attributes:
          container.id:
            enabled: false
        collection_interval: 10s

      kubeletstats:
        collection_interval: 20s
        auth_type: "serviceAccount"
        endpoint: "https://172.21.207.99:10250"
        insecure_skip_verify: true
        extra_metadata_labels:
          - container.id
          - k8s.volume.type
        metric_groups:
          - container
          - node
          - pod
          - volume
          
    exporters:
      datadog/aio:
        api:
          site: datadoghq.com	
          key: "e9f690f62ffbf0c41c9354649ff0f005"
        metrics:
          resource_attributes_as_tags: true
          instrumentation_scope_metadata_as_tags: true
        host_metadata:
          enabled: true

      datadog/metrics:
        api:
          site: datadoghq.com	
          key: "25e72d23f81dafb038fdedbb4ac62847"
        metrics:
          resource_attributes_as_tags: true
          instrumentation_scope_metadata_as_tags: true
        host_metadata:
          enabled: true

    service:
      pipelines:
        metrics:
          receivers: [ otlp, hostmetrics, prometheus, k8s_cluster, kubeletstats ]
          processors: [ cumulativetodelta, resourcedetection, batch  ]
          exporters: [ datadog/aio, datadog/metrics ]
        logs/entity_events:
          receivers: [ otlp, k8s_cluster ]
          processors: [ resourcedetection, batch  ]
          exporters: [ datadog/aio ]
        logs:
          receivers: [ otlp, k8s_events, k8sobjects ]
          processors: [ resourcedetection, batch  ]
          exporters: [ datadog/aio ]
        traces:
          receivers: [ otlp ]
          processors: [ resourcedetection, batch  ]
          exporters: [ datadog/aio ]
```

## Configure Automatic Instrumentation 

The following command will create a basic Instrumentation resource that is configured specifically for instrumenting the services.
```sh
# opentelemetry-instrumentation.yml
apiVersion: opentelemetry.io/v1alpha1
kind: Instrumentation
metadata:
  name: otel-instrumentation
spec:
  exporter:
    endpoint: http://otel-collector.default.svc.cluster.local:4318
  propagators:
    - tracecontext
    - baggage
    - b3
  sampler:
    type: parentbased_traceidratio
    argument: "1"
  python:
    env:
      # Required if endpoint is set to 4317.
      # Python autoinstrumentation uses http/proto by default
      # so data must be sent to 4318 instead of 4317.
      - name: OTEL_EXPORTER_OTLP_ENDPOINT
        value: http://otel-collector.default.svc.cluster.local:4318
      - name: OTEL_EXPORTER_OTLP_PROTOCOL
        value: http/protobuf
  dotnet:
    env:
      # Required if endpoint is set to 4317.
      # Dotnet autoinstrumentation uses http/proto by default
      # See https://github.com/open-telemetry/opentelemetry-dotnet-instrumentation/blob/888e2cd216c77d12e56b54ee91dafbc4e7452a52/docs/config.md#otlp
      - name: OTEL_EXPORTER_OTLP_ENDPOINT
        value: http://otel-collector.default.svc.cluster.local:4318
      - name: OTEL_EXPORTER_OTLP_PROTOCOL
        value: http/protobuf
      - name: OTEL_ASPNETCORE_HOSTINGSTARTUPASSEMBLIES
        value: OpenTelemetry.AutoInstrumentation.AspNetCoreBootstrapper
  java:
    env:
      # Required if endpoint is set to 4317.
      # Go autoinstrumentation uses http/proto by default
      # so data must be sent to 4318 instead of 4317.
      - name: OTEL_EXPORTER_OTLP_ENDPOINT
        value: http://otel-collector.default.svc.cluster.local:4318
      - name: OTEL_EXPORTER_OTLP_PROTOCOL
        value: http/protobuf
```
Add annotations to existing deployments 
1. .NET: instrumentation.opentelemetry.io/inject-dotnet: "true"
2. Java: instrumentation.opentelemetry.io/inject-java: "true"
3. Python: instrumentation.opentelemetry.io/inject-python: "true"

By default, Python logs auto-instrumentation is disabled. If you would like to enable this feature, you must to set the OTEL_LOGS_EXPORTER to otlp_proto_http and OTEL_PYTHON_LOGGING_AUTO_INSTRUMENTATION_ENABLED to true in the environment variables.
Troubleshooting

## Did the Instrumentation resource install? 
After installing the Instrumentation resource, verify that it installed correctly by running this command, where <namespace> is the namespace in which the Instrumentation resource is deployed:
```sh
kubectl describe otelinst -n <namespace>
```
---
---
## Manifests

### aspnetapp

Create the following files and use kubectl apply to deploy the ASP .Net sample app.
Using LoadBalancer due to self hosted k3s service. 
```sh
# 02-aspnetapp-services.yml
apiVersion: v1
kind: Service
metadata:
  name: aspnetapp

spec:
  type: LoadBalancer
  ports:
    - protocol: TCP
      name: web
      port: 80
  selector:
    app: aspnetapp
```
```sh
# 03-aspnetapp.yml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: aspnetapp
  labels:
    app: aspnetapp

spec:
  replicas: 1
  selector:
    matchLabels:
      app: aspnetapp
  template:
    metadata:
      labels:
        app: aspnetapp
      annotations:
        instrumentation.opentelemetry.io/inject-dotnet: "true"
        instrumentation.opentelemetry.io/otel-dotnet-auto-runtime: "linux-musl-x64"
    spec:
      containers:
        - name: aspnetapp
          image: mcr.microsoft.com/dotnet/samples:aspnetapp
          imagePullPolicy: IfNotPresent
          #command: ["/bin/sh", "-c"]
          #args: ["apk add curl; curl -sSfL https://raw.githubusercontent.com/open-telemetry/opentelemetry-dotnet-instrumentation/v0.6.0/otel-dotnet-auto-install.sh -O && sh /app/otel-dotnet-auto-install.sh; ls -al; sh /app/instrument.sh"]
          env:
            - name: "ASPNETCORE_HTTP_PORTS"
              value: "80"
            - name: "DOTNET_RUNNING_IN_CONTAINER"
              value: "true"
            - name: "OTEL_DOTNET_AUTO_LOGS_CONSOLE_EXPORTER_ENABLED"
              value: "true"
            - name: "OTEL_DOTNET_AUTO_METRICS_CONSOLE_EXPORTER_ENABLED"
              value: "true"
            - name: "OTEL_DOTNET_AUTO_TRACES_CONSOLE_EXPORTER_ENABLED"
              value: "true"
            - name: "ASPNETCORE_ENVIRONMENT"
              value: "dev-V-SGB2P360-WIN"
          ports:
            - name: web
              containerPort: 80
```

## python-fastapi
Create  the following files and use kubectl apply to deploy the Python sample app.
Using LoadBalancer due to self hosted k3s service.
```sh
# 02-fastapi-services.yml
apiVersion: v1
kind: Service
metadata:
  name: fastapi

spec:
  type: LoadBalancer
  ports:
    - protocol: TCP
      name: web
      port: 80
  selector:
    app: fastapi
```
```sh
# 03-fastapi.yml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: fastapi
  labels:
    app: fastapi

spec:
  replicas: 1
  selector:
    matchLabels:
      app: fastapi
  template:
    metadata:
      labels:
        app: fastapi
      annotations:
        instrumentation.opentelemetry.io/inject-python: "true"
    spec:
      containers:
        - name: fastapi
          image: tiangolo/uvicorn-gunicorn-fastapi
          imagePullPolicy: IfNotPresent
          ports:
            - name: web
              containerPort: 80
```
## Tomcat
Create the following .yaml files and use kubectl apply to deploy the Tomcat container as Java sample app.
Using LoadBalancer due to self hosted k3s service.
```sh
# 02-fastapi-services.yml
apiVersion: v1
kind: Service
metadata:
  name: tomcat

spec:
  type: LoadBalancer
  ports:
    - protocol: TCP
      name: tomcat
      port: 8080
  selector:
    app: tomcat
```
```sh
# 03-fastapi.yml
kind: Deployment
apiVersion: apps/v1
metadata:
  name: tomcat
  labels:
    app: tomcat

spec:
  replicas: 1
  selector:
    matchLabels:
      app: tomcat
  template:
    metadata:
      labels:
        app: tomcat
      annotations:
        instrumentation.opentelemetry.io/inject-java: "true"
    spec:
      containers:
        - name: tomcat
          image: tomcat
          imagePullPolicy: IfNotPresent
          ports:
            - name: tomcat
              containerPort: 8080
```