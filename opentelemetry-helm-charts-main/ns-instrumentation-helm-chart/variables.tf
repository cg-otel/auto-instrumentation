resource "helm_release" "opentelemetry-collector" {
  name       = "opentelemetry-collector"
  namespace   = "test"
  chart      = "opentelemetry-collector/"

  set {
    name  = "config.exporters.datadog.api.site"
    value = "datadoghq.com"
  }

  set {
    name  = "config.exporters.datadog.api.key"
    value = "e9f690f62ffbf0c41c9354649ff0f005"
  }

  set {
    name  = "config.exporters.datadog.metrics.resource_attributes_as_tags"
    value = true
  }

  set {
    name  = "config.exporters.datadog.metrics.instrumentation_scope_metadata_as_tags"
    value = true
  }

  set {
    name  = "config.exporters.datadog.host_metadata.enabled"
    value = true
  }


  set_list {
    name  = "config.service.pipelines.traces.exporters"
    value = [ "datadog" ]
  }

}