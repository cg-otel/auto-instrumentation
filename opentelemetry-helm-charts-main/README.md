# OpenTelemetry Helm Charts

This repository contains [Helm](https://helm.sh/) charts for CG's OpenTelemetry auto-instrumentation project.
