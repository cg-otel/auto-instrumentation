## .NET Runtime Metrics

The following metrics are collected by default after enabling - runtime metrics:

- runtime.dotnet.cpu.system
<br/>	The number of milliseconds executing in the kernel

- runtime.dotnet.cpu.user
<br/>	The number of milliseconds executing outside the kernel

- runtime.dotnet.cpu.percent
<br/>	The percentage of total CPU used by the application

- runtime.dotnet.mem.committed
<br/>	Memory usage

- runtime.dotnet.threads.count
<br/>	The number of threads

- runtime.dotnet.threads.workers_count
<br/>	The number of workers in the threadpool (.NET Core 3.1+ only)

- runtime.dotnet.threads.contention_time
<br/>	The cumulated time spent by threads waiting on a lock (.NET Core 3.1+ only)

- runtime.dotnet.threads.contention_count
<br/>	The number of times a thread stopped to wait on a lock
- runtime.dotnet.exceptions.count
<br/>	The number of first-chance exceptions

- runtime.dotnet.gc.size.gen0
<br/>	The size of the gen 0 heap

- runtime.dotnet.gc.size.gen1
<br/>	The size of the gen 1 heap

- runtime.dotnet.gc.size.gen2
<br/>	The size of the gen 2 heap

- runtime.dotnet.gc.size.loh
<br/>	The size of the large object heap

- runtime.dotnet.gc.memory_load
<br/>	The percentage of the total memory used by the process. The GC changes its behavior when this value gets above 85. (.NET Core 3.1+ only)

- runtime.dotnet.gc.pause_time
<br/>	The amount of time the GC paused the application threads (.NET Core 3.1+ only)

- runtime.dotnet.gc.count.gen0
<br/>	The number of gen 0 garbage collections

- runtime.dotnet.gc.count.gen1
<br/>	The number of gen 1 garbage collections

- runtime.dotnet.gc.count.gen2
<br/>	The number of gen 2 garbage collections

- runtime.dotnet.aspnetcore.requests.total
<br/>	The total number of HTTP requests received by the server (.NET Core 3.1+ only)

- runtime.dotnet.aspnetcore.requests.failed
<br/>	The number of failed HTTP requests received by the server (.NET Core 3.1+ only)

- runtime.dotnet.aspnetcore.requests.current
<br/>	The total number of HTTP requests that have started but not yet stopped (.NET Core 3.1+ only)

- runtime.dotnet.aspnetcore.requests.queue_length
<br/>	The current length of the server HTTP request queue (.NET 5+ only)

- runtime.dotnet.aspnetcore.connections.total
<br/>	The total number of HTTP connections established to the server (.NET 5+ only)

- runtime.dotnet.aspnetcore.connections.current
<br/>	The current number of active HTTP connections to the server (.NET 5+ only)

- runtime.dotnet.aspnetcore.connections.queue_length
<br/>	The current length of the HTTP server connection queue (.NET 5+ only)
